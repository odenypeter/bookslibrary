import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  BASE_URL = 'http://openlibrary.org/'

  constructor(private httpClient: HttpClient) { }

  /**
   * send http request
   * @param url
   * @param method
   * @param body
   */
  public sendRequest(url: string, method: string, body: any): Observable<any> {
    return this.httpClient.request(method, `${this.BASE_URL}${url}`, {body});
  }
}
