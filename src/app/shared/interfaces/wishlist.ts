export interface Wishlist {
  id: string;
  key?: string;
  name: string;
  description?: string;
  tags?: string[] | null;
  seeds?: string[] | null;
  url?: string;
}
