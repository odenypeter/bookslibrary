// create interface for book from openlibrary.org
import {Author} from './author';

export interface Book {
  title: string;
  author_name?: string[];
  authors?: Author[];
  publish_date?: string[];
  number_of_pages?: number;
  key?: string;
  publish_year?: number[];
  cover_i?: string,
  edition_count?: number,
  first_publish_year?: number,
  description?: any;
  cover_img?: string | null;
  subjects?: string[]
  inWishlist?: boolean;
}
