export interface Author {
  alternate_names?: string;
  author_img?: string;
  name: string;
  author?: any;
  title?: string;
  bio?: string;
  wikipedia?: string;
  personal_name?: string;
  birth_date?: string;
  key: string;
  id?: string;
}
