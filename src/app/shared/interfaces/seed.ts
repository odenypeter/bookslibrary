export interface Seed {
  full_url: string;
  last_update: string;
  picture: any;
  title: string;
  type: string;
  url: string;
}
