import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookInWishlistPipe } from './book-in-wishlist/book-in-wishlist.pipe';



@NgModule({
  declarations: [
    BookInWishlistPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
