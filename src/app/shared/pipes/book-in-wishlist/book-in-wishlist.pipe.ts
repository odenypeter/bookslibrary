import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'bookInWishlist'
})
export class BookInWishlistPipe implements PipeTransform {
  transform(value: string, ...args: any): unknown {
    const [wishlist] = args;
    return !!wishlist.seeds[value]
  }

}
