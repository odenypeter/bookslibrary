import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookSummaryCardComponent} from './book-summary-card.component';
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    BookSummaryCardComponent
  ],
  exports: [
    BookSummaryCardComponent
  ],
  imports: [
    CommonModule, RouterModule
  ]
})
export class BookSummaryCardModule {
}
