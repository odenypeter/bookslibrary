import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-book-summary-card',
  templateUrl: './book-summary-card.component.html',
  styleUrls: ['./book-summary-card.component.scss']
})
export class BookSummaryCardComponent {
  @Input() public book: any;
  @Input() public isWishList = false;
  @Input() public wishlist = null;

  @Output() addToWishlist: EventEmitter<string> = new EventEmitter();
  @Output() removeFromWishlist: EventEmitter<string> = new EventEmitter();

}
