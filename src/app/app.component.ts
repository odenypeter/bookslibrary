import {Component, OnInit} from '@angular/core';

import {Store} from '@ngrx/store';

import {WishlistGetActions} from './modules/wishlist/state/wishlist/actions/wishlist.actions';
import {WishlistState} from './modules/wishlist/state/wishlist/reducers/wishlist.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit{

  constructor(private store: Store<WishlistState>) {}
  ngOnInit() {
    // dispatch the action to get the wishlist
    this.store.dispatch(WishlistGetActions.getWishlist());
  }
}
