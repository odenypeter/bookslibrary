import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Subscription, tap} from "rxjs";
import {BookService} from "../../modules/books/services/books/book.service";

interface NavigationLink {
  name: string,
  link: string
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private searchTextSubscription: Subscription = new Subscription();

  public navLinks: NavigationLink[] = [];
  public searchTerm = '';
  private searchParams = {searchTerm: '', searchBy: ''};

  constructor(private router: Router, private bookService: BookService) {
  }

  ngOnInit(): void {
    this.searchTextSubscription = this.bookService.searchText$
      .pipe(tap((searchParams) => {
        console.log('header : ', searchParams);
        // this.searchParams = searchParams;
        this.searchTerm = searchParams.searchTerm
      }))
      .subscribe();

    this.navLinks = [
      {name: 'Home', link: '/books'},
      {name: 'Wishlist', link: '/wishlist'}
    ]
  }

  public search(category: 'title' | 'subject' | 'author') {
    // console.log('searchText : ', this.searchText);

    this.bookService.updateSearchText({
      searchTerm: this.searchTerm,
      searchBy: category,
    });

    this.router.navigate(
      [`/books`],
      {
        queryParams: {
          searchTerm: this.searchTerm,
          searchBy: category,
        },
      }
    ).then();
  }


  ngOnDestroy() {
    if (this.searchTextSubscription) {
      this.searchTextSubscription.unsubscribe();
    }
  }
}
