import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';


import {select, Store} from '@ngrx/store';
import {map, Observable, Subscription, take, tap} from 'rxjs';
import {NotificationsService, NotificationType} from "angular2-notifications";

import {Book} from '../../../../shared/interfaces/book';
import {BookService} from '../../services/books/book.service';
import {WishlistState} from '../../../wishlist/state/wishlist/reducers/wishlist.reducer';
import {WishlistUpdateActions} from '../../../wishlist/state/wishlist/actions/wishlist.actions';
import {Wishlist} from '../../../../shared/interfaces/wishlist';
import {selectCurrentWishlist} from '../../../wishlist/state/wishlist/selectors/wishlist.selectors';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrl: './books-list.component.scss'
})
export class BooksListComponent implements OnInit, OnDestroy {

  @Input() public book: any;

  private searchTextSubscription: Subscription = new Subscription();

  books$: Observable<Book[]> = new Observable<Book[]>();

  public searchParams: any = {searchTerm: '', searchBy: ''};

  currentWishlist: Wishlist = {} as Wishlist

  constructor(
    private bookService: BookService,
    private store: Store<WishlistState>,
    private activatedRoute: ActivatedRoute,
    private notification: NotificationsService,) {
  }

  ngOnInit() {
    this.searchTextSubscription = this.bookService.searchText$
      .pipe(tap((searchParams) => {
        this.searchParams = searchParams;
        this.getBooks(searchParams);
      }))
      .subscribe();

    this.activatedRoute.queryParams.subscribe((searchParams: any) => {
      console.log('searchParams : ', searchParams);

      if (searchParams) {
        if (searchParams.hasOwnProperty('searchTerm') && searchParams.hasOwnProperty('searchBy')) {
          this.bookService.updateSearchText(searchParams);
        }
      }
    });
  }

  ngOnDestroy() {
    if (this.searchTextSubscription) {
      this.searchTextSubscription.unsubscribe();
    }
  }

  public getBooks(searchObject: any) {
    this.store.pipe(
      select(selectCurrentWishlist),
      take(1)
    ).subscribe(wishlist => {
      this.currentWishlist = wishlist || {} as Wishlist
      this.books$ = this.bookService.getBooks(searchObject).pipe(
        map(books => books.map(book => ({
            ...book,
            inWishlist: this.checkBookInWishlist(book)
          }))
        ));
    });
  }

  /**
   * Add book to wishlist
   * @param book
   */
  addBookToList(book: Book) {
    this.store.dispatch(WishlistUpdateActions.updateWishlist({
      wishlist: {
        add: [book]
      },
      listId: this.currentWishlist.id || window.crypto.randomUUID()
    }));
    this.notification.create(
      book.title,
      'Successfully added to wishlist',
      NotificationType.Success
    )
  }

  removeBookFromList(key: string | undefined) {
    this.store.dispatch(WishlistUpdateActions.updateWishlist({
      wishlist: {
        remove: [key]
      },
      listId: this.currentWishlist!.id
    }));

  }

  private checkBookInWishlist(book: Book) {
    // @ts-ignore
    return !!this.currentWishlist.seeds[book.key];
  }
}
