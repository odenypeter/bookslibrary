import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {provideRouter} from '@angular/router';

import {of} from 'rxjs';
import {provideMockStore} from '@ngrx/store/testing';
import {StoreModule} from '@ngrx/store';
import {NotificationsService, SimpleNotificationsModule} from 'angular2-notifications';

import {BooksListComponent} from './books-list.component';
import {BookServiceStub} from '../../../../../tests/service.stubs';
import {BookService} from '../../services/books/book.service';


describe('BooksListComponent', () => {
  let component: BooksListComponent;
  let fixture: ComponentFixture<BooksListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot(provideMockStore),
        SimpleNotificationsModule.forRoot(),
      ],
      declarations: [BooksListComponent],
      providers: [
        NotificationsService,
        provideRouter([]),
        {
          provide: BookService,
          useClass: BookServiceStub
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();

    fixture = TestBed.createComponent(BooksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.books$ = of([]);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
