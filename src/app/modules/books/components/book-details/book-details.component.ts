import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';

import {Observable} from 'rxjs';

import {Book} from '../../../../shared/interfaces/book';
import {BookService} from '../../services/books/book.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.scss'
})
export class BookDetailsComponent implements OnInit {
  public book$ = new Observable<Book | null>();

  constructor(private bookService: BookService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    const key = this.activatedRoute.snapshot.params['id']
    this.book$ = this.bookService.getBookDetails(key);
  }

}
