import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Observable} from 'rxjs';

import {BookService} from '../../services/books/book.service';
import {Author} from '../../../../shared/interfaces/author';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrl: './author.component.scss'
})
export class AuthorComponent implements OnInit {
  public author$ = new Observable<Author | null>();

  constructor(private bookService: BookService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    const key = this.activatedRoute.snapshot.params['id'];
    this.author$ = this.bookService.getAuthorDetails(key);
  }
}
