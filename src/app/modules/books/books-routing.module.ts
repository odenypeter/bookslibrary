import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BookDetailsComponent} from './components/book-details/book-details.component';
import {BooksListComponent} from './components/books-list/books-list.component';
import {AuthorComponent} from './components/author/author.component';
import {BooksComponent} from './books.component';

const routes: Routes = [
  {
    path: '',
    component: BooksComponent,
    children: [
      {path: '', component: BooksListComponent},
      {path: ':id', component: BookDetailsComponent},
      {path: 'author/:id', component: AuthorComponent},
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksRoutingModule {
}
