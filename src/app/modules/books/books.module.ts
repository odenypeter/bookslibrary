import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';

import {SimpleNotificationsModule} from 'angular2-notifications';

import { BooksRoutingModule } from './books-routing.module';
import { BooksComponent } from './books.component';
import { BookDetailsComponent } from './components/book-details/book-details.component';
import { BooksListComponent } from './components/books-list/books-list.component';
import { AuthorComponent } from './components/author/author.component';
import {BookSummaryCardModule} from '../../shared/components/book-summary-card/book-summary-card.module';
import {LoaderModule} from '../../shared/components/loader/loader.module';
import {BreadcrumbsModule} from '../../shared/components/breadcrumbs/breadcrumbs.module';
import {PipesModule} from '../../shared/pipes/pipes.module';


@NgModule({
  declarations: [
    BooksComponent,
    BookDetailsComponent,
    BooksListComponent,
    AuthorComponent
  ],
  imports: [
    CommonModule,
    BooksRoutingModule,
    LoaderModule,
    BreadcrumbsModule,
    BookSummaryCardModule,
    PipesModule,
    SimpleNotificationsModule,
    NgOptimizedImage,
  ]
})
export class BooksModule { }
