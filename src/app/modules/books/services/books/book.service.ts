import {Injectable} from '@angular/core';

import {BehaviorSubject, map, Observable} from 'rxjs';

import {HttpService} from '../../../../shared/services/http/http.service';
import {Book} from '../../../../shared/interfaces/book';
import {Author} from '../../../../shared/interfaces/author';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  // Holds current value of search text
  public searchText$ = new BehaviorSubject<any>(
    {
      searchTerm: 'harry porter',
      searchBy: 'author'
    }
  );

  searchTerm = this.searchText$.asObservable();

  constructor(private httpService: HttpService) {
  }


  /**
   * Set the search text
   */
  public updateSearchText(searchText: any) {
    this.searchText$.next(searchText.toString().trim() ? searchText : 'harry porter');
  }


  public getBooks(searchSpec: any): Observable<Book[]> {
    let queryString = '';
    if (!searchSpec) {
      queryString += '?q=harry_porter';
    } else if (searchSpec.searchBy === 'title') {
      queryString += `?title=${searchSpec.searchTerm}`;
    } else if (searchSpec.searchBy === 'author') {
      queryString += `?author=${searchSpec.searchTerm}`;
    } else {
      queryString += `?q=${searchSpec.searchTerm}`;
    }

    return this.httpService.sendRequest(`search.json${queryString}&limit=9`, 'GET', null).pipe(
      map(
        (response: any) => {
          return response.docs.slice(0, 9).map((book: any) => (
            {
              title: book.title,
              author_name: book.author_name,
              publish_date: book.publish_date,
              number_of_pages: book.number_of_pages,
              isbn: book.isbn,
              key: book.key,
              id: book.key ? book.key.split('/')[2] : null,
              publish_year: book.publish_year,
              cover_i: book.cover_i,
              edition_count: book.edition_count,
              first_publish_year: book.first_publish_year,
              cover_img: book.cover_i ? `https://covers.openlibrary.org/b/id/${book.cover_i}-L.jpg` : null
            }
          ));
        }
      ),
      // tap((books: Book[]) => console.log('Books: ', books))
    );
  }

  public getBookDetails(id: string): Observable<Book> {
    return this.httpService.sendRequest(`works/${id}.json`, 'GET', null).pipe(
      map(
        (response: any) => {
          return {
            title: response.title,
            authors: response.authors,
            key: response.key,
            description: response.description,
            cover_img: response.covers ? `http://covers.openlibrary.org/b/id/${response.covers[0]}-L.jpg` : null,
            subjects: response.subjects
          };
        }
      )
    );
  }

  /**
   * get author details
   */
  public getAuthorDetails(id: string): Observable<Author> {
    return this.httpService.sendRequest(`authors/${id}.json`, 'GET', null).pipe();
  }
}
