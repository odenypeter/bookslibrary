import {createSelector, createFeatureSelector} from '@ngrx/store';

import {WishlistState} from '../reducers/wishlist.reducer';

import {Wishlist} from '../../../../../shared/interfaces/wishlist';


export const selectWishlistState = createFeatureSelector<WishlistState>('wishlists');

export const selectWishlistIds = createSelector(
  selectWishlistState,
  (state: WishlistState) => state.ids
);

export const selectWishlistEntities = createSelector(
  selectWishlistState,
  (state: WishlistState) => state.entities
);

export const selectCurrentWishlist = createSelector(
  selectWishlistState,
  (state: WishlistState) => state.entities[state.ids[0]] as Wishlist
);

export const selectWishlistSeeds = (listId: string) => createSelector(
  selectWishlistState,
  (state: WishlistState) => state.entities[listId]?.seeds
);
