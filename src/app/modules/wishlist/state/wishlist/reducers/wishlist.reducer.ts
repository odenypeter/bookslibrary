import {Wishlist} from '../../../../../shared/interfaces/wishlist';
import {createReducer, on} from '@ngrx/store';
import {createEntityAdapter, EntityState} from '@ngrx/entity';

import {
  WishlistCreateActions,
  WishlistUpdateActions,
  WishlistGetActions,
  WishlistSeedActions
} from '../actions/wishlist.actions'
import {Book} from '../../../../../shared/interfaces/book';

// define the state interface
export interface WishlistState extends EntityState<Wishlist> {
  loading: boolean,
  loaded: boolean,
  creating: boolean,
  created: boolean,
  updating: boolean,
  updated: boolean,
  seedsLoading: boolean,
  seedsLoaded: boolean,
  seeds: Book[],
}

// create entity reducer
const wishlistAdapter = createEntityAdapter<Wishlist>()

// define initial state
export const initialState: WishlistState = wishlistAdapter.getInitialState(
  {
    loading: false,
    loaded: false,
    creating: false,
    created: false,
    updating: false,
    updated: false,
    seedsLoading: false,
    seedsLoaded: false,
    seeds: [],
  }
);


export const wishlistReducer = createReducer(
  initialState,
  on(WishlistGetActions.getWishlist, (state) => ({
    ...state,
    loading: true,
    loaded: false
  })),
  on(WishlistGetActions.getWishlistSuccess, (state, {wishlists}) => {
    return wishlistAdapter.setAll(wishlists, {
      ...state,
      loading: false,
      loaded: true
    })
  }),
  on(WishlistGetActions.getWishlistFail, (state) => ({
    ...state,
    loading: false,
    loaded: false,
  })),
  on(WishlistCreateActions.createWishlist, (state) => ({
    ...state,
    creating: true,
    created: false,
  })),
  on(WishlistCreateActions.createWishlistSuccess, (state, {wishlist}) => {
    return wishlistAdapter.addOne(wishlist, {
      ...state,
      creating: false,
      created: true,
    })
  }),
  on(WishlistCreateActions.createWishlistFail, (state) => ({
    ...state,
    creating: false,
    created: false,
  })),
  on(WishlistUpdateActions.updateWishlist, (state) => ({
    ...state,
    updating: true,
    updated: false,
  })),
  on(WishlistUpdateActions.updateWishlistSuccess, (state, {wishlist}) => {
      return wishlistAdapter.updateOne({id: wishlist.id, changes: wishlist}, {
        ...state,
        updating: false,
        updated: true,
      })
    }
  ),
  on(WishlistUpdateActions.updateWishlistFail, (state) => ({
    ...state,
    updating: false,
    updated: false,
  })),
  on(WishlistSeedActions.getWishlistSeeds, (state) => ({
    ...state,
    seedsLoading: true,
    seedsLoaded: false,
  })),
  on(WishlistSeedActions.getWishlistSeedsSuccess, (state, {seeds}) => {
    return {
      ...state,
      seedsLoading: false,
      seedsLoaded: true,
      seeds: seeds
    }
  }),
  on(WishlistSeedActions.getWishlistSeedsFail, (state) => ({
    ...state,
    seedsLoading: false,
    seedsLoaded: false,
  }))
)
