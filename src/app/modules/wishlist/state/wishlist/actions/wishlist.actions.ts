import {createActionGroup, emptyProps, props} from '@ngrx/store';
import {Wishlist} from '../../../../../shared/interfaces/wishlist';
import {Book} from '../../../../../shared/interfaces/book';


export const WishlistGetActions = createActionGroup(
  {
    source: 'WishlistGet',
    events: {
      'Get Wishlist': emptyProps,
      'Get Wishlist Success': props<{ wishlists: Wishlist[] }>(),
      'Get Wishlist Fail': props<{ error: any }>()
    }
  }
)

export const WishlistCreateActions = createActionGroup({
  source: 'WishlistCreate',
  events: {
    'Create Wishlist': props<{ wishlist: Wishlist }>(),
    'Create Wishlist Success': props<{ wishlist: Wishlist }>(),
    'Create Wishlist Fail': props<{ error: any }>()
  }
});


export const WishlistUpdateActions = createActionGroup(
  {
    source: 'WishlistUpdate',
    events: {
      'Update Wishlist': props<{ wishlist: any, listId: string }>(),
      'Update Wishlist Success': props<{ wishlist: Wishlist }>(),
      'Update Wishlist Fail': props<{ error: any }>()
    }
  }
)


export const WishlistSeedActions = createActionGroup(
  {
    source: 'WishlistSeed',
    events: {
      'Get Wishlist Seeds': props<{ listId: string }>(),
      'Get Wishlist Seeds Success': props<{ seeds: Book[]; }>(),
      'Get Wishlist Seeds Fail': props<{ error: any }>()
    }
  }
)
