import {Injectable} from '@angular/core';
import {Actions, ofType, createEffect} from '@ngrx/effects';
import {of} from 'rxjs';
import {catchError, exhaustMap, map} from 'rxjs/operators';

import {
  WishlistCreateActions,
  WishlistUpdateActions,
  WishlistGetActions,
  WishlistSeedActions
} from '../actions/wishlist.actions';
import {WishlistService} from '../../../services/wishlist/wishlist.service';

@Injectable()
export class WishlistEffects {

  getWishlist$ = createEffect(() => this.actions$.pipe(
    ofType(WishlistGetActions.getWishlist),
    exhaustMap(action => this.wishlistService.getWishlist().pipe(
      map(wishlists => WishlistGetActions.getWishlistSuccess({wishlists})),
      catchError(error => of(WishlistGetActions.getWishlistFail({error})))
    ))
  ))

  createWishlist = createEffect(() =>
    this.actions$.pipe(
      ofType(WishlistCreateActions.createWishlist),
      exhaustMap(action =>
        this.wishlistService.createWishlist(action.wishlist).pipe(
          map(wishlist => WishlistCreateActions.createWishlistSuccess({wishlist})),
          catchError(error => of(WishlistCreateActions.createWishlistFail({error})))
        )
      )
    )
  );

  updateWishlist$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WishlistUpdateActions.updateWishlist),
      exhaustMap(action =>
        this.wishlistService.updateWishlist(action.wishlist, action.listId).pipe(
          map(wishlist => WishlistUpdateActions.updateWishlistSuccess({wishlist})),
          catchError(error => of(WishlistUpdateActions.updateWishlistFail({error})))
        )
      )
    )
  )

  wishlistSeed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WishlistSeedActions.getWishlistSeeds),
      exhaustMap(action =>
        this.wishlistService.getWishlistSeeds(action.listId).pipe(
          map(seeds => WishlistSeedActions.getWishlistSeedsSuccess({seeds})),
          catchError(error => of(WishlistSeedActions.getWishlistSeedsFail({error})))
        )
      )
    )
  )

  constructor(
    private actions$: Actions,
    private wishlistService: WishlistService
  ) {
  }
}
