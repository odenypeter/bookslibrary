import { isDevMode } from '@angular/core';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import {wishlistReducer, WishlistState} from '../wishlist/reducers/wishlist.reducer';

export interface State {
  wishlists: WishlistState,

}

export const reducers: ActionReducerMap<State> = {
  wishlists: wishlistReducer,
};


export const metaReducers: MetaReducer<State>[] = isDevMode() ? [] : [];
