import {Component, OnInit} from '@angular/core';

import {map, Observable, take, tap} from 'rxjs';
import {NotificationsService, NotificationType} from "angular2-notifications";
import {select, Store} from '@ngrx/store';

import {selectCurrentWishlist, selectWishlistSeeds} from './state/wishlist/selectors/wishlist.selectors';
import {
  WishlistCreateActions,
  WishlistSeedActions,
  WishlistUpdateActions
} from './state/wishlist/actions/wishlist.actions';
import {Wishlist} from '../../shared/interfaces/wishlist';
import {Book} from '../../shared/interfaces/book';
import {WishlistState} from './state/wishlist/reducers/wishlist.reducer';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrl: './wishlist.component.scss'
})
export class WishlistComponent implements OnInit {

  public wishListSeeds$ = new Observable<Book[]>();

  private currentWishlist: Wishlist | null = null;

  constructor(private store: Store<WishlistState>, private notification: NotificationsService) {
  }

  ngOnInit() {
    // select the wishlist
    this.store.pipe(
      select(selectCurrentWishlist),
      tap((wishlist: Wishlist) => {
        if (wishlist) {
          // set the current wishlist
          this.currentWishlist = wishlist;

          // dispatch the action to get the wishlist seeds
          this.store.dispatch(WishlistSeedActions.getWishlistSeeds({listId: wishlist.id}));
        }
      }),
      take(1)
    ).subscribe();

    // set the wishlist seeds
    this.wishListSeeds$ = this.store.pipe(
      select(selectWishlistSeeds(this.currentWishlist?.id || 'undefined')),
      map((seeds: any) => Object.values(seeds))
    );

  }

  addSeed() {
    this.store.dispatch(WishlistCreateActions.createWishlist({
      wishlist: {
        name: 'My Wishlist',
        description: 'My first wishlist',
      } as unknown as Wishlist
    }))
  }

  removeBookToList(key: string | undefined) {
    this.store.dispatch(WishlistUpdateActions.updateWishlist({
      wishlist: {
        remove: [key]
      },
      listId: this.currentWishlist!.id
    }));

    this.notification.create('', 'Successfully removed from wishlist', NotificationType.Success)
  }
}
