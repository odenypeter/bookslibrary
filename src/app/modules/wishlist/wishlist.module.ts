import { NgModule } from '@angular/core';
import { WishlistComponent } from './wishlist.component';
import {WishlistRoutingModule} from './wishlist-routing.module';
import {CommonModule} from '@angular/common';
import {BookSummaryCardModule} from '../../shared/components/book-summary-card/book-summary-card.module';
import {LoaderModule} from '../../shared/components/loader/loader.module';
import {SimpleNotificationsModule} from 'angular2-notifications';




@NgModule({
  declarations: [
    WishlistComponent
  ],
    imports: [
        CommonModule,
        WishlistRoutingModule,
        BookSummaryCardModule,
        LoaderModule,
        SimpleNotificationsModule
    ]
})
export class WishlistModule { }
