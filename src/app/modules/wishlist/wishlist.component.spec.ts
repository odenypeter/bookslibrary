import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import {NotificationsService, SimpleNotificationsModule} from 'angular2-notifications';
import {provideMockStore} from '@ngrx/store/testing';
import {StoreModule} from '@ngrx/store';

import { WishlistComponent } from './wishlist.component';
describe('WishlistComponent', () => {
  let component: WishlistComponent;
  let fixture: ComponentFixture<WishlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StoreModule.forRoot(provideMockStore), SimpleNotificationsModule.forRoot(),],
      providers: [NotificationsService],
      declarations: [WishlistComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();

    fixture = TestBed.createComponent(WishlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
