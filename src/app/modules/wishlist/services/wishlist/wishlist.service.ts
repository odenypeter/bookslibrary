import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {HttpService} from '../../../../shared/services/http/http.service';
import {Wishlist} from '../../../../shared/interfaces/wishlist';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {
  public createWishlist(wishlist: Wishlist): Observable<Wishlist> {
    const lists: any = JSON.parse(localStorage.getItem('wishlist') || '{}');
    lists[wishlist.id] = wishlist;
    localStorage.setItem('wishlist', JSON.stringify(lists));
    return of(wishlist);
  }

  public getWishlist(): Observable<Wishlist[]> {
    const lists: Wishlist[] = Object.values(JSON.parse(localStorage.getItem('wishlist') || '{}'));
    return of(lists);
  }

  public updateWishlist(wishlist: any, listId: any): Observable<Wishlist> {
    const lists = JSON.parse(localStorage.getItem('wishlist') || '{}');
    const currentList = lists[listId] || {id: listId, seeds: {}}
    // update the list
    if (wishlist['add']) {
      let seeds = JSON.parse(JSON.stringify(lists[listId]?.seeds || {}));
      if (Array.isArray(seeds)) {
        seeds = {};
      }
      wishlist['add'].forEach((seed: any) => {
        // add the current item
        seeds[seed.key] = seed
      });
      currentList.seeds = seeds;
    }

    if (wishlist['remove']) {
      const seeds = JSON.parse(JSON.stringify(lists[listId]?.seeds || {}));
      // remove the seeds
      wishlist['remove'].forEach((key: string) => {
        console.log('Removing seed', key);
        delete seeds[key];
      });
      currentList.seeds = seeds;
    }
    lists[listId] = currentList;

    // update the current list
    localStorage.setItem('wishlist', JSON.stringify(lists));
    return of(lists[listId]);
  }

  public getWishlistSeeds(listId: string): Observable<any> {
    console.log('Get Wishlist Seeds was called!!!', listId);
    const list = JSON.parse(localStorage.getItem('wishlist') || '{}')[listId];
    return of(Object.values(list.seeds));
  }
}
