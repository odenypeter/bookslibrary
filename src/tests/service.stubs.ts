import {Observable, of} from 'rxjs';
import {Book} from '../app/shared/interfaces/book';
import {Author} from '../app/shared/interfaces/author';

export class BookServiceStub {
  searchText$: Observable<string> = of('harry potter');
  public getBooks(searchTerm: string): Observable<Book[]> {

    return of(
      [
        {
          title: 'Test title 1',
          author_name: ['John Doe'],
          key: 'works/HJD001',
          first_publish_year: 2012
        },
        {
          title: 'Test title 2',
          author_name: ['John Test'],
          key: 'works/123H)',
          first_publish_year: 2015
        }
      ]
    )
  }

  public getBookDetails(id: string): Observable<Book> {
    return of({
      title: 'Title',
      authors: [{name: 'Author'}] as Author[],
      key: 'key',
      description: 'Description',
      cover_img: 'http://covers.openlibrary.org/b/id/1-L.jpg',
      subjects: ['Subject']
    });
  }

  public getAuthorDetails(id: string): Observable<Author> {
    return of({
      name: 'Author',
      key: 'key',
      bio: 'Bio',
      photos: ['http://covers.openlibrary.org/b/id/1-L.jpg']
    });
  }
}
